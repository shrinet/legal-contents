<?php

namespace Mvs\Legalcontent\Repositories\Legalcontents;

interface LegalcontentsRepositoryInterface
{
    /**
     * Returns a dataset compatible with data grid.
     *
     * @return \Mvs\Legalcontent\Models\Legalcontents
     */
    public function grid();

    /**
     * Returns all the legalcontent entries.
     *
     * @return \Mvs\Legalcontent\Models\Legalcontents
     */
    public function findAll();

    /**
     * Returns a legalcontent entry by its primary key.
     *
     * @param  int  $id
     * @return \Mvs\Legalcontent\Models\Legalcontents
     */
    public function find($id);

    /**
     * Determines if the given legalcontent is valid for creation.
     *
     * @param  array  $data
     * @return \Illuminate\Support\MessageBag
     */
    public function validForCreation(array $data);

    /**
     * Determines if the given legalcontent is valid for update.
     *
     * @param  int  $id
     * @param  array  $data
     * @return \Illuminate\Support\MessageBag
     */
    public function validForUpdate($id, array $data);

    /**
     * Creates or updates the given legalcontent.
     *
     * @param  int  $id
     * @param  array  $input
     * @return bool|array
     */
    public function store($id, array $input);

    /**
     * Creates a legalcontent entry with the given data.
     *
     * @param  array  $data
     * @return \Mvs\Legalcontent\Models\Legalcontents
     */
    public function create(array $data);

    /**
     * Updates the legalcontent entry with the given data.
     *
     * @param  int  $id
     * @param  array  $data
     * @return \Mvs\Legalcontent\Models\Legalcontents
     */
    public function update($id, array $data);

    /**
     * Deletes the legalcontent entry.
     *
     * @param  int  $id
     * @return bool
     */
    public function delete($id);
}
