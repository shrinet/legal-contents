<?php

namespace Mvs\Legalcontent\Repositories\Legalcontents;

use Carbon\Carbon;
use Cartalyst\Support\Traits;
use Symfony\Component\Finder\Finder;
use Illuminate\Contracts\Container\Container;

class LegalcontentsRepository implements LegalcontentsRepositoryInterface
{
    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Data handler.
     *
     * @var \Mvs\Legalcontent\Handlers\Legalcontents\LegalcontentsDataHandlerInterface
     */
    protected $data;

    /**
     * The Eloquent legalcontent model.
     *
     * @var string
     */
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Contracts\Container\Container  $app
     * @return void
     */
    public function __construct(Container $app)
    {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->data = $app['mvs.legalcontent.legalcontents.handler.data'];

        $this->setValidator($app['mvs.legalcontent.legalcontents.validator']);

        $this->setModel(get_class($app['Mvs\Legalcontent\Models\Legalcontents']));
    }

    /**
     * {@inheritDoc}
     */
    public function grid()
    {
        return $this
            ->createModel();
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return $this->container['cache']->rememberForever('mvs.legalcontent.legalcontents.all', function() {
            return $this->createModel()->get();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function find($id)
    {
        return $this->container['cache']->rememberForever('mvs.legalcontent.legalcontents.'.$id, function() use ($id) {
            return $this->createModel()->find($id);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $input)
    {
        return $this->validator->on('create')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpdate($id, array $input)
    {
        return $this->validator->on('update')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function store($id, array $input)
    {
        return ! $id ? $this->create($input) : $this->update($id, $input);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input)
    {
        // Create a new legalcontents
        $legalcontents = $this->createModel();

        // Fire the 'mvs.legalcontent.legalcontents.creating' event
        if ($this->fireEvent('mvs.legalcontent.legalcontents.creating', [ $input ]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForCreation($data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Save the legalcontents
            $legalcontents->fill($data)->save();

            // Fire the 'mvs.legalcontent.legalcontents.created' event
            $this->fireEvent('mvs.legalcontent.legalcontents.created', [ $legalcontents ]);
        }

        return [ $messages, $legalcontents ];
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $input)
    {
        // Get the legalcontents object
        $legalcontents = $this->find($id);

        // Fire the 'mvs.legalcontent.legalcontents.updating' event
        if ($this->fireEvent('mvs.legalcontent.legalcontents.updating', [ $legalcontents, $input ]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForUpdate($legalcontents, $data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Update the legalcontents
            $legalcontents->fill($data)->save();

            // Fire the 'mvs.legalcontent.legalcontents.updated' event
            $this->fireEvent('mvs.legalcontent.legalcontents.updated', [ $legalcontents ]);
        }

        return [ $messages, $legalcontents ];
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id)
    {
        // Check if the legalcontents exists
        if ($legalcontents = $this->find($id))
        {
            // Fire the 'mvs.legalcontent.legalcontents.deleting' event
            $this->fireEvent('mvs.legalcontent.legalcontents.deleting', [ $legalcontents ]);

            // Delete the legalcontents entry
            $legalcontents->delete();

            // Fire the 'mvs.legalcontent.legalcontents.deleted' event
            $this->fireEvent('mvs.legalcontent.legalcontents.deleted', [ $legalcontents ]);

            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function enable($id)
    {
        $this->validator->bypass();

        return $this->update($id, [ 'enabled' => true ]);
    }

    /**
     * {@inheritDoc}
     */
    public function disable($id)
    {
        $this->validator->bypass();

        return $this->update($id, [ 'enabled' => false ]);
    }

    public function getBySlug($slug, $request)
    {
        $data = $this->createModel()->where('slug',$slug)->first();
        $var['start_date'] = Carbon::now()->isoFormat('Do [day of] MMM, YYYY');
        $var['name']       = $request->first_evaluation->full_name;
        $var['adn']        = $request->first_evaluation->memeber_id;

        $data['content'] = $this->prepareContent($data['content'], $var);
        return $data;
    }

    protected function prepareContent($content, $variable)
    {
        $allowed = array("first_name"=>"John","last_name"=>"Smith","status"=>"won");
        foreach ($variable as $key => $val) {
            $content = str_ireplace('{'.$key.'}', $val, $content);
        }
        return $content;
    }

}
