<?php

namespace Mvs\Legalcontent\Validator\Legalcontents;

interface LegalcontentsValidatorInterface
{
    /**
     * Updating a legalcontents scenario.
     *
     * @return void
     */
    public function onUpdate();
}
