<?php

namespace Mvs\Legalcontent\Validator\Legalcontents;

use Cartalyst\Support\Validator;

class LegalcontentsValidator extends Validator implements LegalcontentsValidatorInterface
{
    /**
     * {@inheritDoc}
     */
    protected $rules = [

    ];

    /**
     * {@inheritDoc}
     */
    public function onUpdate()
    {

    }
}
