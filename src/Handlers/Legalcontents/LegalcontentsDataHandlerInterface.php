<?php

namespace Mvs\Legalcontent\Handlers\Legalcontents;

interface LegalcontentsDataHandlerInterface
{
    /**
     * Prepares the given data for being stored.
     *
     * @param  array  $data
     * @return mixed
     */
    public function prepare(array $data);
}
