<?php

namespace Mvs\Legalcontent\Handlers\Legalcontents;

use Mvs\Legalcontent\Models\Legalcontents;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface LegalcontentsEventHandlerInterface extends BaseEventHandlerInterface
{
    /**
     * When a legalcontents is being created.
     *
     * @param  array  $data
     * @return mixed
     */
    public function creating(array $data);

    /**
     * When a legalcontents is created.
     *
     * @param  \Mvs\Legalcontent\Models\Legalcontents  $legalcontents
     * @return mixed
     */
    public function created(Legalcontents $legalcontents);

    /**
     * When a legalcontents is being updated.
     *
     * @param  \Mvs\Legalcontent\Models\Legalcontents  $legalcontents
     * @param  array  $data
     * @return mixed
     */
    public function updating(Legalcontents $legalcontents, array $data);

    /**
     * When a legalcontents is updated.
     *
     * @param  \Mvs\Legalcontent\Models\Legalcontents  $legalcontents
     * @return mixed
     */
    public function updated(Legalcontents $legalcontents);

    /**
     * When a legalcontents is being deleted.
     *
     * @param  \Mvs\Legalcontent\Models\Legalcontents  $legalcontents
     * @return mixed
     */
    public function deleting(Legalcontents $legalcontents);

    /**
     * When a legalcontents is deleted.
     *
     * @param  \Mvs\Legalcontent\Models\Legalcontents  $legalcontents
     * @return mixed
     */
    public function deleted(Legalcontents $legalcontents);
}
