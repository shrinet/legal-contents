<?php

namespace Mvs\Legalcontent\Handlers\Legalcontents;

use Illuminate\Contracts\Events\Dispatcher;
use Mvs\Legalcontent\Models\Legalcontents;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class LegalcontentsEventHandler extends BaseEventHandler implements LegalcontentsEventHandlerInterface
{
    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher)
    {
        $dispatcher->listen('mvs.legalcontent.legalcontents.creating', __CLASS__.'@creating');
        $dispatcher->listen('mvs.legalcontent.legalcontents.created', __CLASS__.'@created');

        $dispatcher->listen('mvs.legalcontent.legalcontents.updating', __CLASS__.'@updating');
        $dispatcher->listen('mvs.legalcontent.legalcontents.updated', __CLASS__.'@updated');

        $dispatcher->listen('mvs.legalcontent.legalcontents.deleted', __CLASS__.'@deleting');
        $dispatcher->listen('mvs.legalcontent.legalcontents.deleted', __CLASS__.'@deleted');
    }

    /**
     * {@inheritDoc}
     */
    public function creating(array $data)
    {

    }

    /**
     * {@inheritDoc}
     */
    public function created(Legalcontents $legalcontents)
    {
        $this->flushCache($legalcontents);
    }

    /**
     * {@inheritDoc}
     */
    public function updating(Legalcontents $legalcontents, array $data)
    {

    }

    /**
     * {@inheritDoc}
     */
    public function updated(Legalcontents $legalcontents)
    {
        $this->flushCache($legalcontents);
    }

    /**
     * {@inheritDoc}
     */
    public function deleting(Legalcontents $legalcontents)
    {

    }

    /**
     * {@inheritDoc}
     */
    public function deleted(Legalcontents $legalcontents)
    {
        $this->flushCache($legalcontents);
    }

    /**
     * Flush the cache.
     *
     * @param  \Mvs\Legalcontent\Models\Legalcontents  $legalcontents
     * @return void
     */
    protected function flushCache(Legalcontents $legalcontents)
    {
        $this->app['cache']->forget('mvs.legalcontent.legalcontents.all');

        $this->app['cache']->forget('mvs.legalcontent.legalcontents.'.$legalcontents->id);
    }
}
