<?php

namespace Mvs\Legalcontent\Models;

use Cartalyst\Attributes\EntityInterface;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Platform\Attributes\Traits\EntityTrait;

class Legalcontents extends Model implements EntityInterface
{
    use EntityTrait, NamespacedEntityTrait, SoftDeletes;

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'mvs/legalcontent.legalcontents';
    /**
     * {@inheritDoc}
     */
    protected $table = 'legalcontents';
    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

}
