<?php

namespace Mvs\Legalcontent\Providers;

use Cartalyst\Support\ServiceProvider;

class LegalcontentsServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {


        // Subscribe the registered event handler
        $this->app['events']->subscribe('mvs.legalcontent.legalcontents.handler.event');
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
        // Register the repository
        $this->bindIf('mvs.legalcontent.legalcontents', 'Mvs\Legalcontent\Repositories\Legalcontents\LegalcontentsRepository');

        // Register the data handler
        $this->bindIf('mvs.legalcontent.legalcontents.handler.data', 'Mvs\Legalcontent\Handlers\Legalcontents\LegalcontentsDataHandler');

        // Register the event handler
        $this->bindIf('mvs.legalcontent.legalcontents.handler.event', 'Mvs\Legalcontent\Handlers\Legalcontents\LegalcontentsEventHandler');

        // Register the validator
        $this->bindIf('mvs.legalcontent.legalcontents.validator', 'Mvs\Legalcontent\Validator\Legalcontents\LegalcontentsValidator');
    }
}
