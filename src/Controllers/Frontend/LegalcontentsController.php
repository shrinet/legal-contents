<?php

namespace Mvs\Legalcontent\Controllers\Frontend;

use Platform\Foundation\Controllers\Controller;

class LegalcontentsController extends Controller
{
    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('mvs/legalcontent::index');
    }
}
