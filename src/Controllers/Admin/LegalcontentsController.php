<?php

namespace Mvs\Legalcontent\Controllers\Admin;

use Illuminate\Container\Container;
use Platform\Access\Controllers\AdminController;
use Cartalyst\DataGrid\Export\Providers\ExportProvider;
use Mvs\Legalcontent\Repositories\Legalcontents\LegalcontentsRepositoryInterface;

class LegalcontentsController extends AdminController
{
    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * The Legalcontent repository.
     *
     * @var \Mvs\Legalcontent\Repositories\Legalcontents\LegalcontentsRepositoryInterface
     */
    protected $legalcontents;

    protected $app;

    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
        'enable',
        'disable',
    ];

    /**
     * Constructor.
     *
     * @param  \Mvs\Legalcontent\Repositories\Legalcontents\LegalcontentsRepositoryInterface  $legalcontents
     * @return void
     */
    public function __construct(Container $app, LegalcontentsRepositoryInterface $legalcontents)
    {
        parent::__construct();

        $this->app = $app;

        $this->legalcontents = $legalcontents;

        config('platform.themes.config.active.admin') == 'admin::atomy';
        $this->app['themes']->setActive('admin::atomy');
    }

    /**
     * Display a listing of legalcontents.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('mvs/legalcontent::legalcontents.index');
    }

    /**
     * Datasource for the legalcontents Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid()
    {
        $settings = [
            'columns' => [
                'id',
				'name',
				'slug',
				'content',
				'created_at',
            ],
            'sorts' => [
                'column'    => 'created_at',
                'direction' => 'desc',
            ],
            'transformer' => function($element) {
                $element->edit_uri = route('admin.mvs.legalcontent.legalcontents.edit', $element->id);

                return $element;
            },
        ];

        // Create export provider
        $provider = new ExportProvider();

        return datagrid($this->legalcontents->grid(), $settings, $provider);
    }

    /**
     * Show the form for creating new legalcontents.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Handle posting of the form for creating new legalcontents.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        return $this->processForm('create');
    }

    /**
     * Show the form for updating legalcontents.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('update', $id);
    }

    /**
     * Handle posting of the form for updating legalcontents.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * Remove the specified legalcontents.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $type = $this->legalcontents->delete($id) ? 'success' : 'error';

        $this->alerts->{$type}(
            trans("mvs/legalcontent::legalcontents/message.{$type}.delete")
        );

        return redirect()->route('admin.mvs.legalcontent.legalcontents.all');
    }

    /**
     * Executes the mass action.
     *
     * @return \Illuminate\Http\Response
     */
    public function executeAction()
    {
        $action = request()->input('action');

        if (in_array($action, $this->actions)) {
            foreach (request()->input('rows', []) as $row) {
                $this->legalcontents->{$action}($row);
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        // Do we have a legalcontents identifier?
        if (isset($id)) {
            if ( ! $legalcontents = $this->legalcontents->find($id)) {
                $this->alerts->error(trans('mvs/legalcontent::legalcontents/message.not_found', compact('id')));

                return redirect()->route('admin.mvs.legalcontent.legalcontents.all');
            }
        } else {
            $legalcontents = $this->legalcontents->createModel();
        }

        // Show the page
        return view('mvs/legalcontent::legalcontents.form', compact('mode', 'legalcontents'));
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {
        // Store the legalcontents
        list($messages) = $this->legalcontents->store($id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("mvs/legalcontent::legalcontents/message.success.{$mode}"));

            return redirect()->route('admin.mvs.legalcontent.legalcontents.all');
        }

        $this->alerts->error($messages, 'form');

        return redirect()->back()->withInput();
    }
}
