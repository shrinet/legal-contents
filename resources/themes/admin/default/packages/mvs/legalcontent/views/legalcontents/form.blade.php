@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans("action.{$mode}") }}} {{ trans('mvs/legalcontent::legalcontents/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('czMore', 'mvs/legalcontent::js/jquery.czMore.js', 'jquery') }}
{{ Asset::queue('script', 'mvs/legalcontent::js/script.js', 'czMore') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
    <style type="text/css">
        .btnPlus {
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
        }
        .btnPlus:before {
            content: "\f067";
        }
    </style>
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('page')

<section class="panel panel-default panel-tabs">

    {{-- Form --}}
    <form id="legalcontent-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate>

        {{-- Form: CSRF Token --}}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <header class="panel-heading">

            <nav class="navbar navbar-default navbar-actions">

                <div class="container-fluid">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.mvs.legalcontent.legalcontents.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
                            <i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
                        </a>

                        <span class="navbar-brand">{{{ trans("action.{$mode}") }}} <small>{{{ $legalcontents->exists ? $legalcontents->id : null }}}</small></span>
                    </div>

                    {{-- Form: Actions --}}
                    <div class="collapse navbar-collapse" id="actions">

                        <ul class="nav navbar-nav navbar-right">

                            @if ($legalcontents->exists)
                            <li>
                                <a href="{{ route('admin.mvs.legalcontent.legalcontents.delete', $legalcontents->id) }}" class="tip" data-action-delete data-toggle="tooltip" data-original-title="{{{ trans('action.delete') }}}" type="delete">
                                    <i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.delete') }}}</span>
                                </a>
                            </li>
                            @endif

                            <li>
                                <button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
                                    <i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
                                </button>
                            </li>

                        </ul>

                    </div>

                </div>

            </nav>

        </header>

        <div class="panel-body">

            <div role="tabpanel">

                {{-- Form: Tabs --}}
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('mvs/legalcontent::legalcontents/common.tabs.general') }}}</a></li>
                    <li role="presentation"><a href="#attributes" aria-controls="attributes" role="tab" data-toggle="tab">{{{ trans('mvs/legalcontent::legalcontents/common.tabs.attributes') }}}</a></li>
                </ul>

                <div class="tab-content">

                    {{-- Tab: General --}}
                    <div role="tabpanel" class="tab-pane fade in active" id="general-tab">

                        <fieldset>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                                        <label for="name" class="control-label">
                                            <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('mvs/legalcontent::legalcontents/model.general.name_help') }}}"></i>
                                            {{{ trans('mvs/legalcontent::legalcontents/model.general.name') }}}
                                        </label>

                                        <input type="text" class="form-control" name="name" id="name" placeholder="{{{ trans('mvs/legalcontent::legalcontents/model.general.name') }}}" value="{{{ old('name', $legalcontents->name) }}}">

                                        <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group{{ Alert::onForm('slug', ' has-error') }}">

                                        <label for="slug" class="control-label">
                                            <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('mvs/legalcontent::legalcontents/model.general.slug_help') }}}"></i>
                                            {{{ trans('mvs/legalcontent::legalcontents/model.general.slug') }}}
                                        </label>

                                        <input type="text" class="form-control" name="slug" id="slug" data-slugify="#slug" placeholder="{{{ trans('mvs/legalcontent::legalcontents/model.general.slug') }}}" value="{{{ old('slug', $legalcontents->slug) }}}">

                                        <span class="help-block">{{{ Alert::onForm('slug') }}}</span>

                                    </div>
                                </div>





								<div class="form-group{{ Alert::onForm('content', ' has-error') }}">

                                    <label for="content" class="control-label">
                                        <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('mvs/legalcontent::legalcontents/model.general.content_help') }}}"></i>
                                        {{{ trans('mvs/legalcontent::legalcontents/model.general.content') }}}
                                    </label>

                                    <textarea class="form-control" name="content" id="content" placeholder="{{{ trans('mvs/legalcontent::legalcontents/model.general.content') }}}">{{{ old('content', $legalcontents->content) }}}</textarea>

                                    <span class="help-block">{{{ Alert::onForm('content') }}}</span>




                                    <div id="czContainer">
                                        <div id="first">
                                            <div class="recordset">
                                                <div class="form-group">
                                                    <label for="content" class="control-label">
                                                        Block Title
                                                    </label>
                                                    <input type="text" class="form-control" name="block_title">
                                                </div>
                                                <div class="form-group">
                                                    <textarea class="form-control" name="block_content" placeholder="Block Content"></textarea>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>

                        </fieldset>

                    </div>



                </div>

            </div>

        </div>

    </form>

</section>
@stop
