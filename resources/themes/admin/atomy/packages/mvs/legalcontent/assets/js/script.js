var Extension;

;(function(window, document, $, undefined) {
    'use strict';

    Extension = Extension || {
        Form: {},
    };

    // Initialize functions
    Extension.Form.init = function () {
        Extension.Form
            .redactor()
            //.czMore()


        ;
    }

    Extension.Form.czMore = function () {
        $("#czContainer").czMore({
            styleOverride: true,

            onLoad: function(index) {
                console.log("index", index)
                Extension.Form.redactor()
                //$("block_"+index+"_title").val("yourvalue");
                //Mainly used for then you have a number of record sets that are passed/loaded into html
                //before trigering the czMore plugin, so when looping on the recordsets this evenT
                //will trigger each time it finds an container with id=recordset
            },
            onAdd: function (index) {
                $.fn.redactor('.redactor','destroy')
                $.fn.redactor('#block_'+index+'_content')
                //Extension.Form.redactor()
                //console.log($.fn.redactor('.redactor','destroy'))
            }
        });
    }

    // Initialize Redactor: http://imperavi.com/redactor
    Extension.Form.redactor = function()
    {
        if ($.fn.redactor)
        {
            var csrf_token = $('meta[name="csrf-token"]').attr('content');

            $('.redactor').redactor({
                minHeight: 200,
                buttonSource: true,
                toolbarFixed: true,
                imageUpload: '/admin/media/upload_redactor',
                fileUpload: '/admin/media/upload_redactor',
                uploadImageFields: {
                    '_token': csrf_token
                },
                uploadFileFields: {
                    '_token': csrf_token
                },
                imageManagerJson: '/admin/media/images_list',
                fileManagerJson: '/admin/media/files_list',
                plugins: ['imagemanager', 'filemanager']
            });
        }

        return this;
    };


    // Job done, lets run.
    Extension.Form.init();

})(window, document, jQuery);
