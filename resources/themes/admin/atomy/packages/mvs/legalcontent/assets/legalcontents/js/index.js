var Extension;

;(function(window, document, $, undefined)
{
    'use strict';

    Extension = Extension || {
        Index: {},
    };

    // Initialize functions
    Extension.Index.init = function()
    {
        Extension.Index
            .datePicker()
            .dataGrid()
            .listeners()
        ;
    };

    // Add Listeners
    Extension.Index.listeners = function()
    {
        Platform.Cache.$body
            .on('click', '[data-grid-row]', Extension.Index.checkRow)
            .on('click', '[data-grid-row] a', Extension.Index.titleClick)
            .on('click', '[data-grid-checkbox]', Extension.Index.checkboxes)
            .on('click', '#modal-confirm button.confirm', Extension.Index.bulkActions)
            .on('click', '[data-grid-calendar-preset]', Extension.Index.calendarPresets)
            .on('click', '[data-grid-bulk-action]:not([data-grid-bulk-action="delete"])', Extension.Index.bulkActions)
        ;

        return this;
    };

    // Handle Data Grid checkboxes
    Extension.Index.checkboxes = function(event)
    {
        event.stopPropagation();

        var type = $(this).attr('data-grid-checkbox');

        if (type === 'all')
        {
            $('[data-grid-checkbox]').not(this).prop('checked', this.checked);

            $('[data-grid-row]').not(this).toggleClass('active', this.checked);
        }

        $(this).parents('[data-grid-row]').toggleClass('active');

        Extension.Index.bulkStatus();
    };

    // Handle Data Grid row checking
    Extension.Index.checkRow = function()
    {
        $(this).toggleClass('active');

        var checkbox = $(this).find('[data-grid-checkbox]');

        checkbox.prop('checked', ! checkbox.prop('checked'));

        Extension.Index.bulkStatus();
    };

    Extension.Index.bulkStatus = function()
    {
        var rows = $('[data-grid-checkbox]').not('[data-grid-checkbox="all"]').not('[data-grid-checkbox][disabled]').length;

        var checked = $('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]').not('[data-grid-checkbox][disabled]').length;

        $('[data-grid-bulk-action]').closest('li').toggleClass('disabled', ! checked);

        if (checked > 0)
        {
            $('[data-grid-bulk-action="delete"]').attr('data-modal', true);
        }
        else
        {
            $('[data-grid-bulk-action="delete"]').removeAttr('data-modal');
        }

        $('[data-grid-checkbox="all"]')
            .prop('disabled', rows < 1)
            .prop('checked', rows < 1 ? false : rows === checked)
        ;

        return this;
    };

    Extension.Index.exporterStatus = function(grid)
    {
        $('[data-grid-exporter]').closest('li').toggleClass('disabled', grid.pagination.filtered == 0);

        return this;
    };

    // Handle Data Grid bulk actions
    Extension.Index.bulkActions = function(event)
    {
        event.preventDefault();

        var url = window.location.origin + window.location.pathname;

        var action = $(this).data('grid-bulk-action') ? $(this).data('grid-bulk-action') : 'delete';

        var rows = $.map($('[data-grid-checkbox]:checked').not('[data-grid-checkbox="all"]'), function(event)
        {
            return +event.value;
        });

        if (rows.length > 0)
        {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    action : action,
                    rows   : rows
                },
                success: function(response)
                {
                    Extension.Index.Grid.refresh(true);
                }
            });
        }
    };

    // Handle Data Grid calendar
    Extension.Index.calendarPresets = function(event)
    {
        event.preventDefault();

        var start, end;

        switch ($(this).data('grid-calendar-preset'))
        {
            case 'day':
                start = end = moment().subtract(1, 'day').startOf('day').format('YYYY-MM-DD');
            break;

            case 'week':
                start = moment().startOf('week').format('YYYY-MM-DD');
                end   = moment().endOf('week').format('YYYY-MM-DD');
            break;

            case 'month':
                start = moment().startOf('month').format('YYYY-MM-DD');
                end   = moment().endOf('month').format('YYYY-MM-DD');
            break;

            default:
        }

        $('input[name="daterangepicker_start"]').val(start);

        $('input[name="daterangepicker_end"]').val(end);

        $('.range_inputs .applyBtn').trigger('click');
    };

    // Ignore row selection on title click
    Extension.Index.titleClick = function(event)
    {
        event.stopPropagation();
    };

    // Date range picker initialization
    Extension.Index.datePicker = function()
    {
        var startDate, endDate, config, filter;

        var filters = _.compact(
            String(window.location.hash.slice(1)).split('/')
        );

        config = {
            opens: 'left'
        };

        _.each(filters, function(route)
        {
            filter = route.split(':');

            if (filter[0] === 'created_at' && filter[1] !== undefined && filter[2] !== undefined)
            {
                startDate = moment(filter[1]);

                endDate = moment(filter[2]);
            }
        });

        if (startDate && endDate)
        {
            config = {
                startDate: startDate,
                endDate: endDate,
                opens: 'left',
            };
        }

        Platform.Cache.$body.on('click', '.range_inputs .applyBtn', function()
        {
            $('input[name="daterangepicker_start"]').trigger('change');
        });

        Extension.Index.datePicker = $('[data-grid-calendar]').daterangepicker(config, function(start, end, label)
        {
            $('input[name="daterangepicker_start"]').trigger('change');
        });

        $('.daterangepicker_start_input').attr('data-grid', 'legalcontents');

        $('.daterangepicker_end_input').attr('data-grid', 'legalcontents');

        $('input[name="daterangepicker_start"]')
            .attr('data-grid-type', 'range')
            .attr('data-grid-query', 'created_at:>:' + $('input[name="daterangepicker_start"]').val())
            .attr('data-grid-range', 'start')
            .attr('data-grid-filter', 'created_at')
            .attr('data-grid-label', 'Created At');

        $('input[name="daterangepicker_end"]')
            .attr('data-grid-type', 'range')
            .attr('data-grid-query', 'created_at:<:' + $('input[name="daterangepicker_end"]').val())
            .attr('data-grid-range', 'end')
            .attr('data-grid-filter', 'created_at')
            .attr('data-grid-label', 'Created At');

        return this;
    };

    // Data Grid initialization
    Extension.Index.dataGrid = function()
    {
        var config = {
            scroll: '#data-grid',
            events: {
                removing: function(dg)
                {
                    _.each(dg.applied_filters, function(filter)
                    {
                        if (filter.column === 'created_at' && filter.from !== undefined && filter.to !== undefined)
                        {
                            $('[data-grid-calendar]').val('');
                        }
                    });
                }
            },
            callback: function()
            {
                $('[data-grid-checkbox-all]').prop('checked', false);

                $('[data-action]').prop('disabled', true);

                Extension.Index
                    .bulkStatus()
                    .exporterStatus(this)
                ;
            }
        };

        var dg = new DataGridManager();

        Extension.Index.Grid = dg.create('legalcontents', config);

        return this;
    };

    // Job done, lets run.
    Extension.Index.init();

})(window, document, jQuery);
