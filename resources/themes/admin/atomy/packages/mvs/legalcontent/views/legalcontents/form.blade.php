@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans("action.{$mode}") }}} {{ trans('mvs/legalcontent::legalcontents/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('redactor', 'redactor/css/redactor.css', 'styles') }}

{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('slugify', 'platform/js/slugify.js', 'jquery') }}
{{ Asset::queue('redactor', 'redactor/js/redactor.min.js', 'jquery') }}
{{ Asset::queue('czMore', 'mvs/legalcontent::js/jquery.czMore.js', 'jquery') }}
{{ Asset::queue('script', 'mvs/legalcontent::js/script.js', 'czMore') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
    <style type="text/css">
        .btnPlus, .btnMinus {
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
        }
        .btnPlus:before {
            content: "\f067";
        }
        .btnMinus:before {
            content: "\f068";
        }
        p{
            font-weight: inherit;
        }
    </style>
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('page')

<section class="customer-dashboard">
    <div class="middle-section bg-grey">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12 text-color-primary">
                    <h2 class="back">
                      <span>
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                        <a href="{{ route('admin.mvs.legalcontent.legalcontents.all') }}">Go Back</a>
                      </span>
                    </h2>
                </div>
            </div>

            <div class="row">

                <div class="col-md-10 col-md-offset-1">

                    <div class="row m-0 mt-4 mb-4 form-box">
                        {{-- Form --}}
                        <form id="legalcontent-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate>

                            {{-- Form: CSRF Token --}}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="bg-grey form-inner" style="height: auto;">

                            <fieldset>

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                                            <label for="name" class="control-label">
                                                <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('mvs/legalcontent::legalcontents/model.general.name_help') }}}"></i>
                                                {{{ trans('mvs/legalcontent::legalcontents/model.general.name') }}}
                                            </label>

                                            <input type="text" class="form-control" name="name" id="name" placeholder="{{{ trans('mvs/legalcontent::legalcontents/model.general.name') }}}" value="{{{ old('name', $legalcontents->name) }}}">

                                            <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group{{ Alert::onForm('slug', ' has-error') }}">

                                            <label for="slug" class="control-label">
                                                <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('mvs/legalcontent::legalcontents/model.general.slug_help') }}}"></i>
                                                {{{ trans('mvs/legalcontent::legalcontents/model.general.slug') }}}
                                            </label>

                                            <input type="text" class="form-control" name="slug" id="slug" data-slugify="#slug" placeholder="{{{ trans('mvs/legalcontent::legalcontents/model.general.slug') }}}" value="{{{ old('slug', $legalcontents->slug) }}}">

                                            <span class="help-block">{{{ Alert::onForm('slug') }}}</span>

                                        </div>
                                    </div>



                                    <div class="col-md-12">
                                    <div class="form-group{{ Alert::onForm('content', ' has-error') }}">

                                        <label for="content" class="control-label">
                                            <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('mvs/legalcontent::legalcontents/model.general.content_help') }}}"></i>
                                            {{{ trans('mvs/legalcontent::legalcontents/model.general.content') }}}
                                        </label>


                                        <textarea class="form-control redactor" name="content" id="content" placeholder="{{{ trans('mvs/legalcontent::legalcontents/model.general.content') }}}">{{{ old('content', $legalcontents->content) }}}</textarea>

                                    </div>
                                    </div>


                                </div>

                            </fieldset>
                            </div>
                            <button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
                                <i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@stop
