@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{ trans('mvs/legalcontent::legalcontents/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('bootstrap-daterange', 'bootstrap/css/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('moment', 'moment/js/moment.js', 'jquery') }}
{{ Asset::queue('lodash', 'cartalyst/js/lodash.min.js') }}
{{ Asset::queue('exojs', 'cartalyst/js/exoskeleton.min.js', 'lodash') }}
{{ Asset::queue('data-grid', 'cartalyst/js/data-grid.js', 'jquery') }}
{{ Asset::queue('index', 'mvs/legalcontent::legalcontents/js/index.js', 'platform') }}
{{ Asset::queue('bootstrap-daterange', 'bootstrap/js/daterangepicker.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('page')

{{-- Grid --}}
<section class="panel panel-default panel-grid" data-grid="legalcontents">

    {{-- Grid: Header --}}
    <header class="panel-heading">

        <nav class="navbar navbar-default navbar-actions">

            <div class="container-fluid">

                <div class="navbar-header">

                    <span class="navbar-brand">{{{ trans('mvs/legalcontent::legalcontents/common.title') }}}</span>
                </div>

                <a href="{{ route('admin.mvs.legalcontent.legalcontents.create') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.create') }}}">
                    <i class="fa fa-plus"></i> <span class="visible-xs-inline">{{{ trans('action.create') }}}</span>
                </a>



            </div>

        </nav>

    </header>

    <div class="panel-body">

        {{-- Grid: Applied Filters --}}
        <div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

            <div id="data-grid_applied" class="btn-group" data-grid-layout="filters"></div>

        </div>

    </div>

    {{-- Grid: Table --}}
    <div class="table-responsive">

        <table id="data-grid" class="table table-hover" data-grid-source="{{ route('admin.mvs.legalcontent.legalcontents.grid') }}">
            <thead>
                <tr>
                    <th><input data-grid-checkbox="all" type="checkbox"></th>

					<th class="sortable" data-grid-sort="name">{{{ trans('mvs/legalcontent::legalcontents/model.general.name') }}}</th>
					<th class="sortable" data-grid-sort="slug">{{{ trans('mvs/legalcontent::legalcontents/model.general.slug') }}}</th>

					<th class="sortable" data-grid-sort="created_at">{{{ trans('mvs/legalcontent::legalcontents/model.general.created_at') }}}</th>
                </tr>
            </thead>
            <tbody data-grid-layout="results"></tbody>
        </table>

    </div>

    <footer class="panel-footer clearfix">

        {{-- Grid: Pagination --}}
        <div id="data-grid_pagination" data-grid-layout="pagination"></div>

    </footer>

    {{-- Grid: templates --}}
    @include('mvs/legalcontent::legalcontents/grid/index/results')
    @include('mvs/legalcontent::legalcontents/grid/index/pagination')
    @include('mvs/legalcontent::legalcontents/grid/index/filters')
    @include('mvs/legalcontent::legalcontents/grid/index/no_results')

</section>

@help('mvs/legalcontent::legalcontents/help')

@stop
