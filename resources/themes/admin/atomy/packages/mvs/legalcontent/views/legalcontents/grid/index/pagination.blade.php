<script type="text/template" data-grid="legalcontents" data-grid-template="pagination">

    <div class="pull-left">

        <div class="pages">
            {{{ trans('common.showing') }}} <%= pagination.pageStart %> {{{ trans('common.to') }}} <%= pagination.pageLimit %> {{{ trans('common.of') }}} <span class="total"><%= pagination.filtered %></span>
        </div>

    </div>

    <div class="pull-right">

        <ul class="pagination pagination-sm">

            <% if (pagination.previousPage !== null) { %>

                <li><a href="#" data-grid="legalcontents" data-grid-page="1"><i class="fa fa-angle-double-left"></i></a></li>

                <li><a href="#" data-grid="legalcontents" data-grid-page="<%= pagination.previousPage %>"><i class="fa fa-chevron-left"></i></a></li>

            <% } else { %>

                <li class="disabled"><span><i class="fa fa-angle-double-left"></i></span></li>

                <li class="disabled"><span><i class="fa fa-chevron-left"></i></span></li>

            <% } %>

            <%

            var num_pages = 11,
                split    = num_pages - 1,
                middle   = Math.floor(split / 2);

            var i = pagination.page - middle > 0 ? pagination.page - middle : 1,
                j = pagination.pages;

            j = pagination.page + middle > pagination.pages ? j : pagination.page + middle;

            i = j - i < split ? j - split : i;

            if (i < 1)
            {
                i = 1;
                j = pagination.pages > split ? split + 1 : pagination.pages;
            }

            %>

            <% for(i; i <= j; i++) { %>

                <% if (pagination.page === i) { %>

                <li class="active"><span><%= i %></span></li>

                <% } else { %>

                <li><a href="#" data-grid="legalcontents" data-grid-page="<%= i %>"><%= i %></a></li>

                <% } %>

            <% } %>

            <% if (pagination.nextPage !== null) { %>

                <li><a href="#" data-grid="legalcontents" data-grid-page="<%= pagination.nextPage %>"><i class="fa fa-chevron-right"></i></a></li>

                <li><a href="#" data-grid="legalcontents" data-grid-page="<%= pagination.pages %>"><i class="fa fa-angle-double-right"></i></a></li>

            <% } else { %>

                <li class="disabled"><span><i class="fa fa-chevron-right"></i></span></li>

                <li class="disabled"><span><i class="fa fa-angle-double-right"></i></span></li>

            <% } %>

        </ul>

    </div>

</script>
