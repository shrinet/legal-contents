<?php

return [

	// General messages
	'not_found' => 'Legalcontents [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Legalcontents was successfully created.',
		'update' => 'Legalcontents was successfully updated.',
		'delete' => 'Legalcontents was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the legalcontents. Please try again.',
		'update' => 'There was an issue updating the legalcontents. Please try again.',
		'delete' => 'There was an issue deleting the legalcontents. Please try again.',
	],

];
