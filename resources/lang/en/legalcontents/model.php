<?php

return [

	'general' => [

		'id' => 'Id',
		'name' => 'Name',
		'slug' => 'Slug',
		'content' => 'Content',
		'created_at' => 'Created At',
		'name_help' => 'Enter the Name here',
		'slug_help' => 'Enter the Slug here',
		'content_help' => 'Enter the Content here',

	],

];
